<?php

namespace AppBundle\Controller;

use AppBundle\Form\GoogleSearchType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $form = $this->createForm(new GoogleSearchType(), null, array(
            'action' => $this->generateUrl('google_search'),
            'method' => 'POST',
        ));

        return $this->render('default/index.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/search", name="google_search")
     */
    public function googleSearchAction(Request $request)
    {
        $cityResults = $stateResults = $zipcodeResults = $phoneNumberResults = array();
        $form = $this->createForm(new GoogleSearchType());
        $form->handleRequest($request);

        if ($form->isValid()) {
            $curlService = $this->get('service_container')->get('get_content.via_curl');
            $data = $form->getData();
            $curlService->setCurrentFormData($data);
            $businessName = 'query=' . $data['businessName'];
            $address = !empty($data['address']) ? str_replace(' ', '+', $data['address']) : '';
            $addressParams = $businessName . '+' . $address;

            if (!empty($data['city'])) {
                $cityParams = $businessName . '+' . $data['city'];
                $curlService->setCityParams($cityParams);
                $cityResults = $curlService->parseCurlAddressContent('city');
            }

            if (!empty($data['state'])) {
                $stateParams = $businessName . '+' . $data['state'];
                $curlService->setStateParams($stateParams);
                $stateResults = $curlService->parseCurlAddressContent('state');
            }

            if (!empty($data['zipcode'])) {
                $zipcodeParams = $businessName . '+' . $data['zipcode'];
                $curlService->setZipcodeParams($zipcodeParams);
                $zipcodeResults = $curlService->parseCurlAddressContent('zipcode');
            }

            if (!empty($data['phoneNumber'])) {
                $phoneNumberParams = $businessName . '+' . $data['phoneNumber'];
                $curlService->setPhoneNumberParams($phoneNumberParams);
                $phoneNumberResults = $curlService->parseCurlAddressContent('phoneNumber');
            }

            $curlService->setAddressParams($addressParams);
            $results = $curlService->parseCurlAddressContent();

            $fullResults = array_merge($results, $cityResults, $stateResults, $zipcodeResults, $phoneNumberResults);
        }

        return $this->render('default/results.html.twig', array(
            'results' => $fullResults,
            'search_data' => $data,
        ));
    }
}
