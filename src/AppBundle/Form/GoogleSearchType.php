<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class GoogleSearchType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('businessName', 'text', array(
                'label' => 'Business Name',
                'attr'  => array(
                    'class' => 'form-control',
                    'placeholder' => 'Business Name',
                ),
            ))
            ->add('address', 'text', array(
                'label' => 'Address',
                'attr'  => array(
                    'class' => 'form-control',
                    'placeholder' => 'Address',
                ),
                'required' => false,
            ))
            ->add('city', 'text', array(
                'label' => 'City',
                'attr'  => array(
                    'class' => 'form-control',
                    'placeholder' => 'City',
                ),
                'required' => false,
            ))
            ->add('state', 'text', array(
                'label' => 'State',
                'attr'  => array(
                    'class' => 'form-control',
                    'placeholder' => 'State',
                ),
                'required' => false,
            ))
            ->add('zipcode', 'text', array(
                'label' => 'ZIP',
                'attr'  => array(
                    'class' => 'form-control',
                    'placeholder' => 'ZIP',
                ),
                'required' => false,
            ))
            ->add('phoneNumber', 'text', array(
                'label' => 'Phone Number',
                'attr'  => array(
                    'class' => 'form-control',
                    'placeholder' => 'Phone Number',
                ),
                'required' => false,
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'google_search';
    }
}
