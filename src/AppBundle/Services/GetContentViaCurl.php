<?php

namespace AppBundle\Services;

use Symfony\Component\DependencyInjection\Container;

class GetContentViaCurl
{
    private $url;

    private $addressParams;

    private $cityParams;

    private $stateParams;

    private $zipcodeParams;

    private $phoneNumberParams;

    private $placeDetailsParams;

    private $currentFormData;

    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function getContainer()
    {
        return $this->container;
    }

    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setCurrentFormData($currentFormData)
    {
        $this->currentFormData = $currentFormData;

        return $this;
    }

    public function getCurrentFormData()
    {
        return $this->currentFormData;
    }

    public function setAddressParams($params)
    {
        $this->addressParams = $params;

        return $this;
    }

    public function getAddressParams()
    {
        return $this->addressParams;
    }

    public function setCityParams($params)
    {
        $this->cityParams = $params;

        return $this;
    }

    public function getCityParams()
    {
        return $this->cityParams;
    }

    public function setStateParams($params)
    {
        $this->stateParams = $params;

        return $this;
    }

    public function getStateParams()
    {
        return $this->stateParams;
    }

    public function setZipcodeParams($params)
    {
        $this->zipcodeParams = $params;

        return $this;
    }

    public function getZipcodeParams()
    {
        return $this->zipcodeParams;
    }

    public function setPhoneNumberParams($params)
    {
        $this->phoneNumberParams = $params;

        return $this;
    }

    public function getPhoneNumberParams()
    {
        return $this->phoneNumberParams;
    }

    public function setPlaceDetailsParams($params)
    {
        $this->placeDetailsParams = $params;

        return $this;
    }

    public function getPlaceDetailsParams()
    {
        return $this->placeDetailsParams;
    }

    public function getGoogleApiBusinessAddressUrl()
    {
        return $this->container->getParameter('google_place_textsearch_url');
    }

    public function getGoogleApiKey()
    {
        return $this->container->getParameter('google_api_key');
    }

    public function curlExecute($url, $params)
    {
        if ($curl = curl_init()) {
            curl_setopt($curl, CURLOPT_URL, $url . $params . '&key=' . $this->getGoogleApiKey());
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $results = curl_exec($curl);
            curl_close($curl);

            return $results;
        }

        return false;
    }

    public function parseCurlAddressContent($category = null, $params = null)
    {
        $places = array();
        $key = 0;

        if (!$params) {
            switch ($category) {
                case 'city':
                    $params = $this->getCityParams();
                    break;

                case 'state':
                    $params = $this->getStateParams();
                    break;

                case 'zipcode':
                    $params = $this->getZipcodeParams();
                    break;

                case 'phoneNumber':
                    $params = $this->getPhoneNumberParams();
                    break;

                default:
                    $params = $this->getAddressParams();
                    break;
            }
        }

        $results = $this->curlExecute($this->getGoogleApiBusinessAddressUrl(), $params);
        $result = json_decode($results);

        foreach($result->results as $item) {
            $placeInfo = $item;
            $places[$key] = array(
                'name' => isset($placeInfo->name) ? $placeInfo->name : '',
                'city' => isset($placeInfo->vicinity) ? $placeInfo->vicinity : '',
                'full_address' => isset($placeInfo->formatted_address) ? $this->checkMissingData($placeInfo->formatted_address) : '',
                'location' => isset($placeInfo->geometry->location) ? $placeInfo->geometry->location : '',
                'phone_number' => isset($placeInfo->international_phone_number) ? $this->checkMissingData($placeInfo->international_phone_number) : '',
                'postal_code' => isset($placeInfo->postal_code) ? $placeInfo->postal_code : '',
            );

            if (isset($placeInfo->address_components)) {
                foreach ($placeInfo->address_components as $component) {
                    if (in_array('postal_code', $component->types)) {
                        $places[$key]['postal_code'] = $component->short_name;
                    }

                    if (in_array('administrative_area_level_1', $component->types)) {
                        $places[$key]['state'] = $component->short_name;
                    }
                }
            }

            $key++;
        }

        return $places;
    }

    private function checkMissingData($str)
    {
        $currentData = $this->getCurrentFormData();
        $address = explode(', ', $str);

        foreach ($address as $key => $addressElement) {
            $simpleElement = explode(' ', $addressElement);

            if (is_array($simpleElement)) {
                foreach ($simpleElement as $simpleKey => $element) {
                    if (!in_array($element, $currentData)) {
                        $simpleElement[$simpleKey] = '<span class="bg-red">' . $element . '</span>';
                    }
                    else {
                        $simpleElement[$simpleKey] = '<span class="bg-green">' . $element . '</span>';
                    }
                }

                $address[$key] = implode(' ', $simpleElement);
            }
            else {
                if (!in_array($simpleElement, $currentData)) {
                    $address[$key] = '<span class="bg-red">' . $element . '</span>';
                }
                else {
                    $address[$key] = '<span class="bg-green">' . $element . '</span>';
                }
            }
        }

        return implode(', ', $address);
    }
}
