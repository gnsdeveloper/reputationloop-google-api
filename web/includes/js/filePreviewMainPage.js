jQuery(function($) {
/// определение jQuery - начало


    //объявлем 2 переменных в которые будет помещён массив файлов после выбора в поле <input type="file">

    var otzivi_files;
    var otzivi_files_tmp = [];
    var photo_files;
    var photo_files_tmp = [];
    var signature_files;
    var signature_files_tmp = [];
    var galery_files;
    var galery_files_tmp = [];
    var tour_files;
    var tour_files_tmp = [];

    $('input[type=file][id=adminbundle_mainpage_presentation]').on('change', prepareUpload);
    $('input[type=file][id=adminbundle_mainpage_photo]').on('change', prepareUploadPhoto);
    $('input[type=file][id=adminbundle_mainpage_signature]').on('change', prepareUploadSignature);
    $('input[type=file][id=adminbundle_tour_galery]').on('change', prepareUploadGalery);
    $('input[type=file][id=adminbundle_tour_photo]').on('change', prepareUploadPhotoTour);
    $('input[type=file][id=adminbundle_portfolio_photo]').on('change', prepareUploadPortfolioPhoto);
    $('input[type=file][id=adminbundle_portfolio_galery]').on('change', prepareUploadPortfolioGalery);

    function prepareUpload(event) {
        /*
         одна переменная временная
         в неё заносится список полученные сразу после выбора файлов
         вторая содержит весь список включая и файлы выбранные ранее
         */
        // присвоение только что выбранных файлов
        otzivi_files = event.target.files;


        // обновление общего массива файлов
        $(otzivi_files).each(function () {
            otzivi_files_tmp = otzivi_files_tmp.concat(this);
        });

        // перерисовка списка после выбора дополнительных файлов
        $(function () {
            repaint_uplod_items();
        });
    }

    function prepareUploadPhoto(event) {
        /*
         одна переменная временная
         в неё заносится список полученные сразу после выбора файлов
         вторая содержит весь список включая и файлы выбранные ранее
         */
        // присвоение только что выбранных файлов
        photo_files = event.target.files;


        // обновление общего массива файлов
        $(photo_files).each(function () {
            photo_files_tmp = photo_files_tmp.concat(this);
        });

        // перерисовка списка после выбора дополнительных файлов
        $(function () {
            repaint_uplod_photo_items();
        });
    }

    function prepareUploadSignature(event) {
        /*
         одна переменная временная
         в неё заносится список полученные сразу после выбора файлов
         вторая содержит весь список включая и файлы выбранные ранее
         */
        // присвоение только что выбранных файлов
        signature_files = event.target.files;


        // обновление общего массива файлов
        $(signature_files).each(function () {
            signature_files_tmp = signature_files_tmp.concat(this);
        });

        // перерисовка списка после выбора дополнительных файлов
        $(function () {
            repaint_uplod_signature_items();
        });
    }
    function prepareUploadGalery(event) {
        /*
         одна переменная временная
         в неё заносится список полученные сразу после выбора файлов
         вторая содержит весь список включая и файлы выбранные ранее
         */
        // присвоение только что выбранных файлов
        galery_files = event.target.files;


        // обновление общего массива файлов
        $(galery_files).each(function () {
            galery_files_tmp = galery_files_tmp.concat(this);
        });

        // перерисовка списка после выбора дополнительных файлов
        $(function () {
            repaint_uplod_galery_items();
        });
    }
    function prepareUploadPhotoTour(event) {
        /*
         одна переменная временная
         в неё заносится список полученные сразу после выбора файлов
         вторая содержит весь список включая и файлы выбранные ранее
         */
        // присвоение только что выбранных файлов
        tour_files = event.target.files;


        // обновление общего массива файлов
        $(tour_files).each(function () {
            tour_files_tmp = tour_files_tmp.concat(this);
        });

        // перерисовка списка после выбора дополнительных файлов
        $(function () {
            repaint_uplod_photo_tour_items();
        });
    }


    function prepareUploadPortfolioGalery(event) {
        /*
         одна переменная временная
         в неё заносится список полученные сразу после выбора файлов
         вторая содержит весь список включая и файлы выбранные ранее
         */
        // присвоение только что выбранных файлов
        galery_files = event.target.files;


        // обновление общего массива файлов
        $(galery_files).each(function () {
            galery_files_tmp = galery_files_tmp.concat(this);
        });

        // перерисовка списка после выбора дополнительных файлов
        $(function () {
            repaint_uplod_portfolio_galery_items();
        });
    }
    function prepareUploadPortfolioPhoto(event) {
        /*
         одна переменная временная
         в неё заносится список полученные сразу после выбора файлов
         вторая содержит весь список включая и файлы выбранные ранее
         */
        // присвоение только что выбранных файлов
        tour_files = event.target.files;


        // обновление общего массива файлов
        $(tour_files).each(function () {
            tour_files_tmp = tour_files_tmp.concat(this);
        });

        // перерисовка списка после выбора дополнительных файлов
        $(function () {
            repaint_uplod_portfolio_photo_items();
        });
    }


    /*
     эта функция отслеживает изменение поля <input type="file">
     и добавляет в общий массив выбранные файлы посредством обратного вызова
     */


    function repaint_uplod_items() {
        /*
         этот код помещён в функцию
         так как:
         после удаления элемента, происходит изменение индексов в массиве otzivi_files_tmp
         у кнопки "удалить" остаются старые индексы
         в этом случае уже нельзя правильно удалить элементы кнопками
         из-за нарушения первоначальных индексов
         поэтому, после удаления элемента, следует перерисовать их список заново.
         */
        // выбираем контейнер для списка
        var fileList = $('div[class=fileList][id=adminbundle_mainpage_presentation]');

        // очищаем от предыдущего кода
        $(fileList).html('');
        // формируем визуальную таблицу из массива файлов
        var table = '<table align="center" width = "100%" border = "0" cellspacing = "0" cellpadding = "0">';
        for (var i=0; i < otzivi_files_tmp.length; i++) {

            // устанавливаем ссылку на текущий объект

            var this_obj = otzivi_files_tmp[i];
            //console.log(otzivi_files_tmp[0]);
            // получаем ссылку на картинку для использования в img src
            try {

                /*
                 из-за бага, в сафари этот метод не работает
                 поэтому в случае неудачи, нужно пропустить этот шаг
                 ссылка на баг https://bugs.webkit.org/show_bug.cgi?id=101671
                 пример, аналогичного скрипта,
                 который тоже не работает в сафари http://blueimp.github.io/jQuery-File-Upload/
                 */

                img_src = window.URL.createObjectURL(this_obj);
            } catch (e) {
            }


            // формируем таблицу
            // js не поддерживает переносы строки, поэтому пришлось вот так её рисовать

            table += '<tr>';
            table += '<td width="120">';
            table += '<div class="image_thummb_document">';
            table += '</div><br>';
            table += '</td>';
            table += '<td>' + this_obj.name + '<br /> '+kilob(this_obj.size)+' </td>';
            table += '<td width="100"><a class="del_uplod_document btn btn-danger" href="#' + i + '">удалить</a></td>';
            table += '</tr>';
            if ('undefined' != typeof img_src) img_src.onload = function () {
                window.URL.revokeObjectURL(img_src);
            } // освобождаем память выделенную под картинку

        }
        table += '</table>';
        $(fileList).html(table); // выгружаем в контейнер созданную визуальную таблицу
        if (this_obj) {
            $('span[id=adminbundle_mainpage_presentation]').css('display', 'none');
        }
    }

    function repaint_uplod_photo_items() {
        /*
         этот код помещён в функцию
         так как:
         после удаления элемента, происходит изменение индексов в массиве otzivi_files_tmp
         у кнопки "удалить" остаются старые индексы
         в этом случае уже нельзя правильно удалить элементы кнопками
         из-за нарушения первоначальных индексов
         поэтому, после удаления элемента, следует перерисовать их список заново.
         */
        // выбираем контейнер для списка
        var fileList = $('div[class=fileList][id=adminbundle_mainpage_photo]');

        // очищаем от предыдущего кода
        $(fileList).html('');
        // формируем визуальную таблицу из массива файлов
        var table = '<table align="center" width = "100%" border = "0" cellspacing = "0" cellpadding = "0">';
        for (var i=0; i < photo_files_tmp.length; i++) {

            // устанавливаем ссылку на текущий объект

            var this_obj = photo_files_tmp[i];
            //console.log(otzivi_files_tmp[0]);
            // получаем ссылку на картинку для использования в img src
            try {

                /*
                 из-за бага, в сафари этот метод не работает
                 поэтому в случае неудачи, нужно пропустить этот шаг
                 ссылка на баг https://bugs.webkit.org/show_bug.cgi?id=101671
                 пример, аналогичного скрипта,
                 который тоже не работает в сафари http://blueimp.github.io/jQuery-File-Upload/
                 */

                img_src = window.URL.createObjectURL(this_obj);
            } catch (e) {
            }


            // формируем таблицу
            // js не поддерживает переносы строки, поэтому пришлось вот так её рисовать

            table += '<tr>';
            table += '<td width="120">';
            table += '<div class="image_thummb">';
            if ('undefined' != typeof img_src) table += '<img src="' + img_src + '" width="100" alt="" border="0" />';
            table += '</div><br>';
            table += '</td>';
            table += '<td>' + this_obj.name + '<br /> '+kilob(this_obj.size)+' </td>';
            table += '<td width="100"><a class="del_uplod_photo btn btn-danger" href="#' + i + '">удалить</a></td>';
            table += '</tr>';
            if ('undefined' != typeof img_src) img_src.onload = function () {
                window.URL.revokeObjectURL(img_src);
            } // освобождаем память выделенную под картинку

        }
        table += '</table>';
        $(fileList).html(table); // выгружаем в контейнер созданную визуальную таблицу
        if (this_obj) {
            $('span[id=adminbundle_mainpage_photo]').css('display', 'none');
        }
    }

    function repaint_uplod_signature_items() {
        /*
         этот код помещён в функцию
         так как:
         после удаления элемента, происходит изменение индексов в массиве otzivi_files_tmp
         у кнопки "удалить" остаются старые индексы
         в этом случае уже нельзя правильно удалить элементы кнопками
         из-за нарушения первоначальных индексов
         поэтому, после удаления элемента, следует перерисовать их список заново.
         */
        // выбираем контейнер для списка
        var fileList = $('div[class=fileList][id=adminbundle_mainpage_signature]');

        // очищаем от предыдущего кода
        $(fileList).html('');
        // формируем визуальную таблицу из массива файлов
        var table = '<table align="center" width = "100%" border = "0" cellspacing = "0" cellpadding = "0">';
        for (var i=0; i < signature_files_tmp.length; i++) {

            // устанавливаем ссылку на текущий объект

            var this_obj = signature_files_tmp[i];
            //console.log(otzivi_files_tmp[0]);
            // получаем ссылку на картинку для использования в img src
            try {

                /*
                 из-за бага, в сафари этот метод не работает
                 поэтому в случае неудачи, нужно пропустить этот шаг
                 ссылка на баг https://bugs.webkit.org/show_bug.cgi?id=101671
                 пример, аналогичного скрипта,
                 который тоже не работает в сафари http://blueimp.github.io/jQuery-File-Upload/
                 */

                img_src = window.URL.createObjectURL(this_obj);
            } catch (e) {
            }


            // формируем таблицу
            // js не поддерживает переносы строки, поэтому пришлось вот так её рисовать

            table += '<tr>';
            table += '<td width="120">';
            table += '<div class="image_thummb">';
            if ('undefined' != typeof img_src) table += '<img src="' + img_src + '" width="100" alt="" border="0" />';
            table += '</div><br>';
            table += '</td>';
            table += '<td>' + this_obj.name + '<br /> '+kilob(this_obj.size)+' </td>';
            table += '<td width="100"><a class="del_uplod_signature btn btn-danger" href="#' + i + '">удалить</a></td>';
            table += '</tr>';
            if ('undefined' != typeof img_src) img_src.onload = function () {
                window.URL.revokeObjectURL(img_src);
            } // освобождаем память выделенную под картинку

        }
        table += '</table>';
        $(fileList).html(table); // выгружаем в контейнер созданную визуальную таблицу
        if (this_obj) {
            $('span[id=adminbundle_mainpage_signature]').css('display', 'none');
        }
    }

    function repaint_uplod_portfolio_galery_items() {
        /*
         этот код помещён в функцию
         так как:
         после удаления элемента, происходит изменение индексов в массиве otzivi_files_tmp
         у кнопки "удалить" остаются старые индексы
         в этом случае уже нельзя правильно удалить элементы кнопками
         из-за нарушения первоначальных индексов
         поэтому, после удаления элемента, следует перерисовать их список заново.
         */
        // выбираем контейнер для списка
        var fileList = $('div[class=fileList][id=adminbundle_portfolio_galery]');

        // очищаем от предыдущего кода
        $(fileList).html('');
        // формируем визуальную таблицу из массива файлов
        var table = '<table align="center" width = "100%" border = "0" cellspacing = "0" cellpadding = "0">';
        for (var i=0; i < galery_files_tmp.length; i++) {

            // устанавливаем ссылку на текущий объект

            var this_obj = galery_files_tmp[i];
            //console.log(otzivi_files_tmp[0]);
            // получаем ссылку на картинку для использования в img src
            try {

                /*
                 из-за бага, в сафари этот метод не работает
                 поэтому в случае неудачи, нужно пропустить этот шаг
                 ссылка на баг https://bugs.webkit.org/show_bug.cgi?id=101671
                 пример, аналогичного скрипта,
                 который тоже не работает в сафари http://blueimp.github.io/jQuery-File-Upload/
                 */

                img_src = window.URL.createObjectURL(this_obj);
            } catch (e) {
            }


            // формируем таблицу
            // js не поддерживает переносы строки, поэтому пришлось вот так её рисовать

            table += '<tr>';
            table += '<td width="120">';
            table += '<div class="image_thummb">';
            if ('undefined' != typeof img_src) table += '<img src="' + img_src + '" width="100" alt="" border="0" />';
            table += '</div><br>';
            table += '</td>';
            table += '<td>' + this_obj.name + '<br /> '+kilob(this_obj.size)+' </td>';
            table += '<td width="100"><a class="del_uplod_portfolio_galery btn btn-danger" href="#' + i + '">удалить</a></td>';
            table += '</tr>';
            if ('undefined' != typeof img_src) img_src.onload = function () {
                window.URL.revokeObjectURL(img_src);
            } // освобождаем память выделенную под картинку

        }
        table += '</table>';
        $(fileList).html(table); // выгружаем в контейнер созданную визуальную таблицу
    }
    function repaint_uplod_portfolio_photo_items() {
        /*
         этот код помещён в функцию
         так как:
         после удаления элемента, происходит изменение индексов в массиве otzivi_files_tmp
         у кнопки "удалить" остаются старые индексы
         в этом случае уже нельзя правильно удалить элементы кнопками
         из-за нарушения первоначальных индексов
         поэтому, после удаления элемента, следует перерисовать их список заново.
         */
        // выбираем контейнер для списка
        var fileList = $('div[class=fileList][id=adminbundle_portfolio_photo]');

        // очищаем от предыдущего кода
        $(fileList).html('');
        // формируем визуальную таблицу из массива файлов
        var table = '<table align="center" width = "100%" border = "0" cellspacing = "0" cellpadding = "0">';
        for (var i=0; i < tour_files_tmp.length; i++) {

            // устанавливаем ссылку на текущий объект

            var this_obj = tour_files_tmp[i];
            //console.log(otzivi_files_tmp[0]);
            // получаем ссылку на картинку для использования в img src
            try {

                /*
                 из-за бага, в сафари этот метод не работает
                 поэтому в случае неудачи, нужно пропустить этот шаг
                 ссылка на баг https://bugs.webkit.org/show_bug.cgi?id=101671
                 пример, аналогичного скрипта,
                 который тоже не работает в сафари http://blueimp.github.io/jQuery-File-Upload/
                 */

                img_src = window.URL.createObjectURL(this_obj);
            } catch (e) {
            }


            // формируем таблицу
            // js не поддерживает переносы строки, поэтому пришлось вот так её рисовать

            table += '<tr>';
            table += '<td width="120">';
            table += '<div class="image_thummb">';
            if ('undefined' != typeof img_src) table += '<img src="' + img_src + '" width="100" alt="" border="0" />';
            table += '</div><br>';
            table += '</td>';
            table += '<td>' + this_obj.name + '<br /> '+kilob(this_obj.size)+' </td>';
            table += '<td width="100"><a class="del_uplod_portfolio_photo btn btn-danger" href="#' + i + '">удалить</a></td>';
            table += '</tr>';
            if ('undefined' != typeof img_src) img_src.onload = function () {
                window.URL.revokeObjectURL(img_src);
            } // освобождаем память выделенную под картинку

        }
        table += '</table>';
        $(fileList).html(table); // выгружаем в контейнер созданную визуальную таблицу
        if (this_obj) {
            $('span[id=adminbundle_portfolio_photo]').css('display', 'none');
        }
    }

    function repaint_uplod_galery_items() {
        /*
         этот код помещён в функцию
         так как:
         после удаления элемента, происходит изменение индексов в массиве otzivi_files_tmp
         у кнопки "удалить" остаются старые индексы
         в этом случае уже нельзя правильно удалить элементы кнопками
         из-за нарушения первоначальных индексов
         поэтому, после удаления элемента, следует перерисовать их список заново.
         */
        // выбираем контейнер для списка
        var fileList = $('.galery');

        // очищаем от предыдущего кода
        $(fileList).html('');
        // формируем визуальную таблицу из массива файлов
        var table = '<table align="center" width = "100%" border = "0" cellspacing = "0" cellpadding = "0">';
        for (var i=0; i < galery_files_tmp.length; i++) {

            // устанавливаем ссылку на текущий объект

            var this_obj = galery_files_tmp[i];
            //console.log(otzivi_files_tmp[0]);
            // получаем ссылку на картинку для использования в img src
            try {

                /*
                 из-за бага, в сафари этот метод не работает
                 поэтому в случае неудачи, нужно пропустить этот шаг
                 ссылка на баг https://bugs.webkit.org/show_bug.cgi?id=101671
                 пример, аналогичного скрипта,
                 который тоже не работает в сафари http://blueimp.github.io/jQuery-File-Upload/
                 */

                img_src = window.URL.createObjectURL(this_obj);
            } catch (e) {
            }


            // формируем таблицу
            // js не поддерживает переносы строки, поэтому пришлось вот так её рисовать

            table += '<tr>';
            table += '<td width="120">';
            table += '<div class="image_thummb">';
            if ('undefined' != typeof img_src) table += '<img src="' + img_src + '" width="100" alt="" border="0" />';
            table += '</div><br>';
            table += '</td>';
            table += '<td>' + this_obj.name + '<br /> '+kilob(this_obj.size)+' </td>';
            table += '<td width="100"><a class="del_uplod_galery btn btn-danger" href="#' + i + '">удалить</a></td>';
            table += '</tr>';
            if ('undefined' != typeof img_src) img_src.onload = function () {
                window.URL.revokeObjectURL(img_src);
            } // освобождаем память выделенную под картинку

        }
        table += '</table>';
        $(fileList).html(table); // выгружаем в контейнер созданную визуальную таблицу
    }
    function repaint_uplod_photo_tour_items() {
        /*
         этот код помещён в функцию
         так как:
         после удаления элемента, происходит изменение индексов в массиве otzivi_files_tmp
         у кнопки "удалить" остаются старые индексы
         в этом случае уже нельзя правильно удалить элементы кнопками
         из-за нарушения первоначальных индексов
         поэтому, после удаления элемента, следует перерисовать их список заново.
         */
        // выбираем контейнер для списка
        var fileList = $('.photo');

        // очищаем от предыдущего кода
        $(fileList).html('');
        // формируем визуальную таблицу из массива файлов
        var table = '<table align="center" width = "100%" border = "0" cellspacing = "0" cellpadding = "0">';
        for (var i=0; i < tour_files_tmp.length; i++) {

            // устанавливаем ссылку на текущий объект

            var this_obj = tour_files_tmp[i];
            //console.log(otzivi_files_tmp[0]);
            // получаем ссылку на картинку для использования в img src
            try {

                /*
                 из-за бага, в сафари этот метод не работает
                 поэтому в случае неудачи, нужно пропустить этот шаг
                 ссылка на баг https://bugs.webkit.org/show_bug.cgi?id=101671
                 пример, аналогичного скрипта,
                 который тоже не работает в сафари http://blueimp.github.io/jQuery-File-Upload/
                 */

                img_src = window.URL.createObjectURL(this_obj);
            } catch (e) {
            }


            // формируем таблицу
            // js не поддерживает переносы строки, поэтому пришлось вот так её рисовать

            table += '<tr>';
            table += '<td width="120">';
            table += '<div class="image_thummb">';
            if ('undefined' != typeof img_src) table += '<img src="' + img_src + '" width="100" alt="" border="0" />';
            table += '</div><br>';
            table += '</td>';
            table += '<td>' + this_obj.name + '<br /> '+kilob(this_obj.size)+' </td>';
            table += '<td width="100"><a class="del_uplod_tour_photo btn btn-danger" href="#' + i + '">удалить</a></td>';
            table += '</tr>';
            if ('undefined' != typeof img_src) img_src.onload = function () {
                window.URL.revokeObjectURL(img_src);
            } // освобождаем память выделенную под картинку

        }
        table += '</table>';
        $(fileList).html(table); // выгружаем в контейнер созданную визуальную таблицу
        if (this_obj) {
            $('span[id=photo]').css('display', 'none');
        }
    }


    /*
     функция которая удаляет из массива элементы
     после этого наглядно показывает удаление элемента из списка fadeOut()
     и перерисовывает таблицу выбранных файлов
     использование $('body').on('click' ... обусловлено тем,
     что ссылки эти создаются динамически на странице, что делает невозможным использование обычного click()
     */
    $('body').on('click', 'a.del_uplod_document', function () {
        if (!confirm('удалить?'))return (false); // переспрашиваем, удалить ли файл из списка
        var to_del_id = $(this).attr('href').replace('#', '') * 1; // получаем индекс удаляемого файла
        if ('undefined' != typeof otzivi_files_tmp && otzivi_files_tmp.length) {
            // проверяем массив и элемент массива на существование
            if ('undefined' != typeof otzivi_files_tmp[to_del_id] && otzivi_files_tmp[to_del_id]) {
                otzivi_files_tmp.splice(to_del_id, 1); // удаляем элемент из массива
                // создаём визуализацию удаления. а затем перерисовываем таблицу обратной функцией
                // если этого не сделать то индексы у кнопок удалить не будут соответствовать индекса массива.
                $(this).parent().parent().fadeOut(function () {
                    repaint_uplod_items();
                    $('span[id=adminbundle_mainpage_presentation]').css('display', '');
                    $('input[type=file][id=adminbundle_mainpage_presentation]').val('');
                });
            }
        }
        return (false);
    });

    $('body').on('click', 'a.del_uplod_photo', function () {
        if (!confirm('удалить?'))return (false); // переспрашиваем, удалить ли файл из списка
        var to_del_id = $(this).attr('href').replace('#', '') * 1; // получаем индекс удаляемого файла
        if ('undefined' != typeof photo_files_tmp && photo_files_tmp.length) {
            // проверяем массив и элемент массива на существование
            if ('undefined' != typeof photo_files_tmp[to_del_id] && photo_files_tmp[to_del_id]) {
                photo_files_tmp.splice(to_del_id, 1); // удаляем элемент из массива
                // создаём визуализацию удаления. а затем перерисовываем таблицу обратной функцией
                // если этого не сделать то индексы у кнопок удалить не будут соответствовать индекса массива.
                $(this).parent().parent().fadeOut(function () {
                    repaint_uplod_items();
                    $('span[id=adminbundle_mainpage_photo]').css('display', '');
                    $('input[type=file][id=adminbundle_mainpage_photo]').val('');
                });
            }
        }
        return (false);
    });

    $('body').on('click', 'a.del_uplod_signature', function () {
        if (!confirm('удалить?'))return (false); // переспрашиваем, удалить ли файл из списка
        var to_del_id = $(this).attr('href').replace('#', '') * 1; // получаем индекс удаляемого файла
        if ('undefined' != typeof signature_files_tmp && signature_files_tmp.length) {
            // проверяем массив и элемент массива на существование
            if ('undefined' != typeof signature_files_tmp[to_del_id] && signature_files_tmp[to_del_id]) {
                signature_files_tmp.splice(to_del_id, 1); // удаляем элемент из массива
                // создаём визуализацию удаления. а затем перерисовываем таблицу обратной функцией
                // если этого не сделать то индексы у кнопок удалить не будут соответствовать индекса массива.
                $(this).parent().parent().fadeOut(function () {
                    repaint_uplod_items();
                    $('span[id=adminbundle_mainpage_signature]').css('display', '');
                    $('input[type=file][id=adminbundle_mainpage_signature]').val('');
                });
            }
        }
        return (false);
    });

    $('body').on('click', 'a.del_uplod_tour_photo', function () {
        if (!confirm('удалить?'))return (false); // переспрашиваем, удалить ли файл из списка
        var to_del_id = $(this).attr('href').replace('#', '') * 1; // получаем индекс удаляемого файла
        if ('undefined' != typeof tour_files_tmp && tour_files_tmp.length) {
            // проверяем массив и элемент массива на существование
            if ('undefined' != typeof tour_files_tmp[to_del_id] && tour_files_tmp[to_del_id]) {
                tour_files_tmp.splice(to_del_id, 1); // удаляем элемент из массива
                // создаём визуализацию удаления. а затем перерисовываем таблицу обратной функцией
                // если этого не сделать то индексы у кнопок удалить не будут соответствовать индекса массива.
                $(this).parent().parent().fadeOut(function () {
                    repaint_uplod_items();
                    $('span[id=photo]').css('display', '');
                    $('input[type=file][id=adminbundle_tour_photo]').val('');
                });
            }
        }
        return (false);
    });

    $('body').on('click', 'a.del_uplod_galery', function () {
        if (!confirm('удалить?'))return (false); // переспрашиваем, удалить ли файл из списка
        var to_del_id = $(this).attr('href').replace('#', '') * 1; // получаем индекс удаляемого файла
        if ('undefined' != typeof galery_files_tmp && galery_files_tmp.length) {
            // проверяем массив и элемент массива на существование
            if ('undefined' != typeof galery_files_tmp[to_del_id] && galery_files_tmp[to_del_id]) {
                galery_files_tmp.splice(to_del_id, 1); // удаляем элемент из массива
                // создаём визуализацию удаления. а затем перерисовываем таблицу обратной функцией
                // если этого не сделать то индексы у кнопок удалить не будут соответствовать индекса массива.
                $(this).parent().parent().fadeOut(function () {
                    repaint_uplod_items();
                    $('input[type=file][id=adminbundle_tour_galery]').val('');
                });
            }
        }
        return (false);
    });

    $('body').on('click', 'a.del_uplod_portfolio_galery', function () {
        if (!confirm('удалить?'))return (false); // переспрашиваем, удалить ли файл из списка
        var to_del_id = $(this).attr('href').replace('#', '') * 1; // получаем индекс удаляемого файла
        if ('undefined' != typeof galery_files_tmp && galery_files_tmp.length) {
            // проверяем массив и элемент массива на существование
            if ('undefined' != typeof galery_files_tmp[to_del_id] && galery_files_tmp[to_del_id]) {
                galery_files_tmp.splice(to_del_id, 1); // удаляем элемент из массива
                // создаём визуализацию удаления. а затем перерисовываем таблицу обратной функцией
                // если этого не сделать то индексы у кнопок удалить не будут соответствовать индекса массива.
                $(this).parent().parent().fadeOut(function () {
                    repaint_uplod_items();
                    $('input[type=file][id=adminbundle_portfolio_galery]').val('');
                });
            }
        }
        return (false);
    });

    $('body').on('click', 'a.del_uplod_portfolio_photo', function () {
        if (!confirm('удалить?'))return (false); // переспрашиваем, удалить ли файл из списка
        var to_del_id = $(this).attr('href').replace('#', '') * 1; // получаем индекс удаляемого файла
        if ('undefined' != typeof tour_files_tmp && tour_files_tmp.length) {
            // проверяем массив и элемент массива на существование
            if ('undefined' != typeof tour_files_tmp[to_del_id] && tour_files_tmp[to_del_id]) {
                tour_files_tmp.splice(to_del_id, 1); // удаляем элемент из массива
                // создаём визуализацию удаления. а затем перерисовываем таблицу обратной функцией
                // если этого не сделать то индексы у кнопок удалить не будут соответствовать индекса массива.
                $(this).parent().parent().fadeOut(function () {
                    repaint_uplod_items();
                    $('span[id=adminbundle_portfolio_photo]').css('display', '');
                    $('input[type=file][id=adminbundle_portfolio_photo]').val('');
                });
            }
        }
        return (false);
    });

// функция округления байтов
    function kilob(val)
    {
        if (val<=1024) 					return(number_format(val,0, '.', ' ')+' б');
        if (val>=1023 && val<1048576) 	return(number_format(val/1024,0, '.', ' ')+' Кб');
        if (val>=1048576) 				return(number_format(val/1048576,1, '.', ' ')+' Мб');
    }


    // функция  number_format алаогичная php функции
    function number_format( number, decimals, dec_point, thousands_sep ) {
        // использование number_format(1234.5678, 2, '.', '');
        var i, j, kw, kd, km;
        if( isNaN(decimals = Math.abs(decimals)) ){
            decimals = 2;
        }
        if( dec_point == undefined )
        {
            dec_point = ",";
        }
        if( thousands_sep == undefined )
        {
            thousands_sep = ".";
        }

        i = parseInt(number = (+number || 0).toFixed(decimals)) + "";

        if( (j = i.length) > 3 ){
            j = j % 3;
        } else{
            j = 0;
        }
        km = (j ? i.substr(0, j) + thousands_sep : "");
        kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
        //kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).slice(2) : "");
        kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");
        return km + kw + kd;
    }

});