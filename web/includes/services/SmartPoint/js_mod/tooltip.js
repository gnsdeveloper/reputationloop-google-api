/**
 * Created by aleksandr on 27.07.15.
 */
/*-----------------------------------------------------------------------------------------------*/
/*                                      SIMPLE jQUERY TOOLTIP                                    */
/*                                      VERSION: 1.1                                             */
/*                                      AUTHOR: jon cazier                                       */
/*                                      EMAIL: jon@3nhanced.com                                  */
/*                                      WEBSITE: 3nhanced.com                                    */
/*-----------------------------------------------------------------------------------------------*/

$(document).ready(function() {
    $('.entypo.circled-info').hover(
        function() {
            this.tip = this.title;
            $(this).append(
                '<div class="entypo circled-infoWrapper">'
                +'<div class="entypo circled-infoTop"></div>'
                +'<div class="entypo circled-infoMid">'
                +this.tip
                +'</div>'
                +'<div class="entypo circled-infoBtm"></div>'
                +'</div>'
            );
            this.title = "";
            this.width = $(this).width();
            $(this).find('.entypo.circled-infoWrapper').css({left:this.width-22})
            $('.entypo.circled-infoWrapper').fadeIn(300);
        },
        function() {
            $('.entypo.circled-infoWrapper').fadeOut(100);
            $(this).children().remove();
            this.title = this.tip;
        }
    );
});